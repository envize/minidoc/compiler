import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import KeywordLexer from "./KeywordLexer";
import LexResult from "./LexResult";

export default class StepTypeLexer extends KeywordLexer implements ISubLexer {
    protected override readonly keywords = [ 'default', 'action' ];
    protected override readonly tokenType = TokenType.StepType;

    public lex(): LexResult {
        const previousToken = this.getPreviousToken();

        if (this.isNaturalStart(previousToken) ||
            previousToken.type === TokenType.Identifier) {
            return super.lex();
        }

        return LexResult.continue;
    }
}

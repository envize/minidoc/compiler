import LexerBase from "../LexerBase";
import Token from "../Token";
import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";

export default class FreeTextLexer extends LexerBase implements ISubLexer {

    public lex(): LexResult {
        const position = this.context.position;
        let result = '';
        while (!this.isEof() &&
            !this.isNaturalEnding() &&
            !this.isWhitespace()) {
            result += this.consume();
        }

        if (result.length > 0) {
            this.context.tokens.push(new Token(TokenType.FreeText, result, position));
            return LexResult.break;
        }

        return LexResult.continue;
    }
}

import LexerBase from "../LexerBase";
import Token from "../Token";
import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import LexResult from "./LexResult";

export default class NewLineLexer extends LexerBase implements ISubLexer {

    public lex(): LexResult {
        if (this.isNewLine()) {
            this.context.tokens.push(new Token(TokenType.NewLine, this.consume(), this.context.position));
            return LexResult.break;
        }

        return LexResult.continue;
    }
}

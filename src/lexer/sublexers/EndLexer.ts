import TokenType from "../TokenType";
import ISubLexer from "./ISubLexer";
import KeywordLexer from "./KeywordLexer";
import LexResult from "./LexResult";

export default class EndLexer extends KeywordLexer implements ISubLexer {
    protected override readonly keywords = [ 'end' ];
    protected override readonly tokenType = TokenType.End;

    public lex(): LexResult {
        if (this.isNaturalStart(this.getPreviousToken())) {
            return super.lex();
        }

        return LexResult.continue;
    }
}

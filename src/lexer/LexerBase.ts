import Context from "./Context";
import Token from "./Token";
import TokenType from "./TokenType";

export default abstract class LexerBase {
    constructor(protected context: Context) { }

    protected get current(): string {
        return this.context.characters[this.context.position];
    }

    protected consume(): string {
        const result = this.current;
        this.context.position++;

        return result;
    }

    protected lookAhead(delta: number): string {
        return this.context.characters[this.context.position + delta] ?? null;
    }

    protected getPreviousToken(): Token {
        return this.context.tokens.length > 0 ?
            this.context.tokens[this.context.tokens.length - 1] :
            new Token(TokenType.BOF, '<BOF>', 0);
    }

    protected isEof(delta = 0): boolean {
        return this.context.position + delta >= this.context.characters.length;
    }

    protected isNewLine(character: string | undefined = undefined): boolean {
        if (character === undefined) {
            character = this.current;
        }

        return character === '\n';
    }

    protected isWhitespace(character: string | undefined = undefined): boolean {
        if (character === undefined) {
            character = this.current;
        }

        return character === ' ';
    }

    protected isNaturalEnding(character: string | undefined = undefined): boolean {
        if (character === undefined) {
            character = this.current;
        }

        return this.isWhitespace(character) ||
            this.isNewLine(character);
    }

    protected isNaturalStart(token: Token): boolean {
        return token.type === TokenType.BOF ||
            token.type === TokenType.NewLine ||
            token.type === TokenType.Indent;
    }
}

import Token from './Token';

export default class Context {
    public tokens: Token[];
    public characters: string[];
    public position: number;

    constructor(characters: string[]) {
        this.tokens = [];
        this.characters = characters;
        this.position = 0;
    }
}

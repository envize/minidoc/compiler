import IParser from './IParser';
import IIntegrator from './IIntegrator';
import ParseResult from './ParseResult';

export default class ParsingPair<TData, TResult, TContext> {

    private constructor(
        public parser: IParser<TData, TResult>,
        public integrator: IIntegrator<TResult, TContext> | null
    ) { }

    public run(data: TData, context: TContext): ParseResult<TResult> {
        const result = this.parser.parse(data);

        if (result.shouldBeIntegrated) {
            this.integrator?.integrate(result, context);
        }

        return result;
    }

    public static create<TData, TResult, TContext>(
        parserAndIntegrator: IParser<TData, TResult> & IIntegrator<TResult, TContext>
    ): ParsingPair<TData, TResult, TContext> {
        return new ParsingPair(parserAndIntegrator, parserAndIntegrator);
    }

    public static createParser<TData, TResult, TContext>(
        parser: IParser<TData, TResult>
    ): ParsingPair<TData, TResult, TContext> {
        return new ParsingPair(parser, null);
    }
}

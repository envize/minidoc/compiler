import Context from '../Context';
import ParserBase from '../ParserBase';
import SectionNode from '../nodes/SectionNode';
import TokenType from '../../lexer/TokenType';
import ParseResult from '../ParseResult';

import FlowSectionParser from './FlowSectionParser';
import MarkdownSectionParser from './MarkdownSectionParser';
import TitleSectionParser from './TitleSectionParser';
import SectionType from '../nodes/Section';
import ParsingPair from '../ParsingPair';
import AbstractSyntaxTree from '../AbstractSyntaxTree';
import IParser from '../IParser';
import IIntegrator from '../IIntegrator';

export default class SectionParser
    extends ParserBase
    implements IParser<undefined, SectionNode | undefined>,
    IIntegrator<SectionNode, AbstractSyntaxTree> {
    private parsers: ParsingPair<SectionType, unknown, SectionNode>[];

    constructor(context: Context) {
        super(context);

        this.parsers = [
            ParsingPair.create(new FlowSectionParser(context)),
            ParsingPair.create(new MarkdownSectionParser(context)),
            ParsingPair.create(new TitleSectionParser(context))
        ];
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public parse(data: undefined): ParseResult<SectionNode | undefined> {
        if (this.current.type === TokenType.Section) {
            const sectionToken = this.consume();
            const sectionType = this.getSectionType(sectionToken.text);

            const node = new SectionNode(sectionType, sectionToken.text);

            for (const parser of this.parsers) {
                const result = parser.run(sectionType, node);

                if (result.breakFromMainLoop) {
                    return ParseResult.break(node);
                }
            }
        }

        return ParseResult.continue();
    }

    public integrate(parseResult: ParseResult<SectionNode>, context: AbstractSyntaxTree): void {
        if (parseResult.shouldBeIntegrated) {
            context.sections.push(parseResult.node);
        }
    }

    // @ts-expect-error no default case
    private getSectionType(keyword: string): SectionType {
        switch (keyword) {
        case '#title':
            return SectionType.Title;
        case '#story':
            return SectionType.Story;
        case '#general':
            return SectionType.General;
        case '#happy-flow':
            return SectionType.HappyFlow;
        case '#alternate-flow':
            return SectionType.AlternateFlow;
        }
    }
}

import Context from '../Context';
import ParserBase from '../ParserBase';
import SectionNode from '../nodes/SectionNode';
import TokenType from '../../lexer/TokenType';
import ParseResult from '../ParseResult';
import FlowSectionNode from '../nodes/FlowSectionNode';
import StartParser from './StartParser';
import StepParser from './StepParser';
import EndParser from './EndParser';
import FlowType from '../nodes/FlowType';
import SectionType from '../nodes/Section';
import IParser from '../IParser';
import ParsingPair from '../ParsingPair';
import ConsumableStartParser from './ConsumableStartParser';
import IIntegrator from '../IIntegrator';
import Token from '../../lexer/Token';

export default class FlowSectionParser
    extends ParserBase
    implements IParser<SectionType, FlowSectionNode | null | undefined>,
    IIntegrator<FlowSectionNode, SectionNode> {
    private parsers: ParsingPair<undefined, unknown, FlowSectionNode>[];

    constructor(context: Context) {
        super(context);

        this.parsers = [
            ParsingPair.createParser(new ConsumableStartParser(context)),
            ParsingPair.create(new StartParser(context)),
            ParsingPair.create(new EndParser(context)),
            ParsingPair.create(new StepParser(context))
        ];
    }

    // todo: assert double start links
    public parse(sectionType: SectionType): ParseResult<FlowSectionNode | null | undefined> {
        if (this.isSectionToParse(sectionType)) {

            const descriptionTokens: Token[] = [];
            while (!this.isNaturalEnd()) { // read description
                descriptionTokens.push(this.consume());
            }

            let description: string | null = null;
            if (descriptionTokens.length > 0) {
                description = descriptionTokens.map(t => t.text).join(' ');
            }

            const flowType = this.getFlowType(sectionType);
            const node = new FlowSectionNode(flowType, description);

            while (!this.isEof() &&
                this.current.type !== TokenType.Section) {

                for (const parser of this.parsers) {
                    const result = parser.run(undefined, node);

                    if (result.breakFromMainLoop) {
                        break;
                    }
                }
            }

            if (node.steps.length === 0) {
                return ParseResult.break(null);
            }

            return ParseResult.break(node);
        }

        return ParseResult.continue();
    }

    public integrate(parseResult: ParseResult<FlowSectionNode>, context: SectionNode): void {
        if (parseResult.shouldBeIntegrated) {
            context.section = parseResult.node;
        }
    }

    // @ts-expect-error no default case
    private getFlowType(sectionType: SectionType): FlowType {
        switch (sectionType) {
        case SectionType.HappyFlow:
            return FlowType.HappyFlow;
        case SectionType.AlternateFlow:
            return FlowType.AlternateFlow;
        }
    }

    private isSectionToParse(sectionType: SectionType): boolean {
        return sectionType === SectionType.HappyFlow ||
            sectionType === SectionType.AlternateFlow;
    }
}

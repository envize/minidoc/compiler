import TokenType from "../../lexer/TokenType";
import IIntegrator from "../IIntegrator";
import IParser from "../IParser";
import FlowSectionNode from '../nodes/FlowSectionNode';
import LinkNode from '../nodes/LinkNode';
import LinkType from "../nodes/LinkType";
import LinkParser from './LinkParser';
import ParseResult from '../ParseResult';
import ParserBase from '../ParserBase';
import Context from '../../parser/Context';

export default class StartParser
    extends ParserBase
    implements IParser<undefined, LinkNode | undefined>,
    IIntegrator<LinkNode, FlowSectionNode> {
    private linkParser: LinkParser;

    constructor(context: Context) {
        super(context);

        this.linkParser = new LinkParser(context);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public parse(data: undefined): ParseResult<LinkNode | undefined> {
        const result = this.linkParser.parse(TokenType.Start, LinkType.Start);
        return result;
    }

    public integrate(parseResult: ParseResult<LinkNode>, context: FlowSectionNode): void {
        if (parseResult.shouldBeIntegrated) {
            context.start = parseResult.node;
        }
    }
}

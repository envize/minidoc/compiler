import ParserBase from '../ParserBase';
import SectionNode from '../nodes/SectionNode';
import ParseResult from '../ParseResult';
import TitleSectionNode from '../nodes/TitleSectionNode';
import TokenType from '../../lexer/TokenType';
import IParser from '../IParser';
import IIntegrator from '../IIntegrator';
import SectionType from '../nodes/Section';

export default class TitleSectionParser
    extends ParserBase
    implements IParser<SectionType, TitleSectionNode | null | undefined>,
    IIntegrator<TitleSectionNode, SectionNode> {

    public parse(sectionType: SectionType): ParseResult<TitleSectionNode | null | undefined> {
        if (sectionType === SectionType.Title) {
            const titleParts = [];

            while (!this.isNaturalEnd()) { // read title
                titleParts.push(this.consume().text);
            }

            while (!this.isEof() &&
                this.current.type !== TokenType.Section) {
                this.consume();
            }

            if (titleParts.length > 0) {
                const title = titleParts.join(' ');
                const node = new TitleSectionNode(title);

                return ParseResult.break(node);
            }

            return ParseResult.break(null);
        }

        return ParseResult.continue();
    }

    public integrate(parseResult: ParseResult<TitleSectionNode>, context: SectionNode): void {
        if (parseResult.shouldBeIntegrated) {
            context.section = parseResult.node;
        }
    }
}

import Token from "../../lexer/Token";
import TokenType from "../../lexer/TokenType";
import LinkNode from "../nodes/LinkNode";
import LinkType from "../nodes/LinkType";
import ParserBase from "../ParserBase";
import ParseResult from "../ParseResult";

export default class LinkParser extends ParserBase {

    public parse(tokenType: TokenType, linkType: LinkType): ParseResult<LinkNode | undefined> {
        if (this.isNaturalBegin(this.getPreviousToken()) &&
            this.current.type === tokenType) {

            // if also has identifier ONLY THEN it is a link node
            if (this.lookAhead(1).type === TokenType.Identifier) { // is link node
                this.consume(); // consume link keyword

                const identifierToken = this.consume(); // consume identifier

                const startDescriptionTokens: Token[] = [];
                while (!this.isNaturalEnd()) { // read description
                    startDescriptionTokens.push(this.consume());
                }

                let description = null;
                if (startDescriptionTokens.length > 0) {
                    description = startDescriptionTokens.map(t => t.text).join(' ');
                }

                const node = new LinkNode(linkType, identifierToken.text);
                node.description = description;

                return ParseResult.break(node);
            }
        }

        return ParseResult.continue();
    }
}

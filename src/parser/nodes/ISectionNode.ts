import SectionType from './SectionType';

export default interface ISectionNode {
    readonly sectionType: SectionType;
}

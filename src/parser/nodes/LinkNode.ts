import LinkType from "./LinkType";

export default class LinkNode {
    public description: string | null = null;

    constructor(
        public type: LinkType,
        public identifier: string
    ) {}
}

enum SectionType {
    Title = "title",
    Markdown = "markdown",
    Flow = "flow"
}

export default SectionType;

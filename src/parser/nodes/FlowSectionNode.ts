import ISectionNode from "./ISectionNode";
import SectionType from "./SectionType";
import LinkNode from './LinkNode';
import StepNode from './StepNode';
import FlowType from "./FlowType";

export default class FlowSectionNode implements ISectionNode {
    public readonly sectionType = SectionType.Flow;

    public steps: StepNode[] = [];
    public start: LinkNode | null = null;
    public end: LinkNode | null = null;

    constructor(
        public type: FlowType,
        public description: string | null
    ) { }
}

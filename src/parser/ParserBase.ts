import Token from '../lexer/Token';
import TokenType from '../lexer/TokenType';
import Context from './Context';

export default abstract class ParserBase {
    constructor(protected context: Context) { }

    protected get current(): Token {
        return this.context.tokens[this.context.position];
    }

    protected consume(): Token {
        const result = this.current;
        this.context.position++;

        return result;
    }

    protected lookAhead(delta: number): Token {
        return this.context.tokens[this.context.position + delta];
    }

    protected getPreviousToken(): Token {
        return this.context.position > 0 ?
            this.context.tokens[this.context.position - 1] :
            new Token(TokenType.BOF, '<BOF>', 0);
    }

    protected isEof(): boolean {
        return this.current.type === TokenType.EOF;
    }

    protected isNewLine(token: Token | undefined = undefined): boolean {
        if (token === undefined) {
            token = this.current;
        }

        return token.type === TokenType.NewLine;
    }

    protected isNaturalEnd(): boolean {
        return this.isEof() || this.isNewLine();
    }

    protected isNaturalBegin(token: Token): boolean {
        return token.type === TokenType.BOF ||
            token.type === TokenType.NewLine ||
            token.type === TokenType.Indent;
    }

    protected isConsumable(): boolean {
        return !this.isEof() &&
            this.current.type !== TokenType.BOF;
    }
}

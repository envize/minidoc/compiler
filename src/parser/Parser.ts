import Token from "../lexer/Token";
import TokenType from "../lexer/TokenType";
import AbstractSyntaxTree from "./AbstractSyntaxTree";
import Context from "./Context";
import SectionNode from "./nodes/SectionNode";
import ParserBase from './ParserBase';
import ParsingPair from "./ParsingPair";
import SectionParser from './subparsers/SectionParser';

export function parse(tokens: Token[]): AbstractSyntaxTree {
    if (tokens === null || tokens === undefined) {
        throw new Error('tokens cannot not be null.');
    }

    return new Parser(tokens).parse();
}

class Parser extends ParserBase {
    private sectionParser: ParsingPair<undefined, SectionNode | undefined, AbstractSyntaxTree>;

    constructor(tokens: Token[]) {
        super(new Context(tokens));

        this.sectionParser = ParsingPair.create(new SectionParser(this.context));
    }

    public parse(): AbstractSyntaxTree {
        const ast = new AbstractSyntaxTree();

        while (!this.isEof()) {

            if (this.current.type !== TokenType.Section) {
                this.consume();
                continue;
            }

            const result = this.sectionParser.run(undefined, ast);

            if (result.breakFromMainLoop) {
                continue;
            }
        }

        return ast;
    }
}

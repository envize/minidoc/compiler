import ParseResult from './ParseResult';

export default interface IIntegrator<TResult, TContext> {
    integrate(parseResult: ParseResult<TResult>, context: TContext): void;
}

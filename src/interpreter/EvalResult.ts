export default class EvalResult {

    public static get break(): EvalResult {
        return new EvalResult(true);
    }

    public static get continue(): EvalResult {
        return new EvalResult(false);
    }

    constructor(public breakFromMainLoop: boolean) {}
}

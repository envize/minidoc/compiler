import EvalResult from '../EvalResult';
import { Document } from "@minidoc/core";

export default interface ISubInterpreter<TNode> {
    eval(doc: Document, node: TNode): EvalResult;
    finalize(doc: Document, node: TNode): EvalResult;
}

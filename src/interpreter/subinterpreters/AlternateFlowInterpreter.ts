import SectionNode from "../../parser/nodes/SectionNode";
import SectionType from "../../parser/nodes/Section";
import EvalResult from "../EvalResult";
import ISubInterpreter from "./ISubInterpreter";
import FlowInterpreter from "./FlowInterpreter";
import FlowSectionNode from "../../parser/nodes/FlowSectionNode";
import { Document } from "@minidoc/core";

export default class AlternateFlowInterpreter
    extends FlowInterpreter
    implements ISubInterpreter<SectionNode> {

    public eval(doc: Document, section: SectionNode): EvalResult {

        if (section.type === SectionType.AlternateFlow) {
            const content = <FlowSectionNode> section.section;

            const flow = this.evalFlow(content);

            if (flow !== undefined && flow !== null) {
                doc.alternateFlows.push(flow);
                return EvalResult.break;
            }
        }

        return EvalResult.continue;
    }

    public finalize(doc: Document, section: SectionNode): EvalResult {
        if (section.type === SectionType.AlternateFlow) {
            this.setLinks(doc, <FlowSectionNode> section.section);
            return EvalResult.break;
        }

        return EvalResult.continue;
    }
}

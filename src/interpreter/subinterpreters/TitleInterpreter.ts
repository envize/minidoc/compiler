import SectionNode from "../../parser/nodes/SectionNode";
import SectionType from "../../parser/nodes/Section";
import EvalResult from "../EvalResult";
import ISubInterpreter from "./ISubInterpreter";
import TitleSectionNode from "../../parser/nodes/TitleSectionNode";
import { Document } from "@minidoc/core";

export default class TitleInterpreter implements ISubInterpreter<SectionNode> {

    public eval(doc: Document, section: SectionNode): EvalResult {

        if (section.type === SectionType.Title) {
            if (doc.title !== undefined) {
                return EvalResult.break;
            }

            const node = <TitleSectionNode>section.section;

            if (node !== null &&
                node.title &&
                node.title.length > 0
            ) {
                doc.title = node.title;
            }

            return EvalResult.break;
        }

        return EvalResult.continue;
    }

    public finalize(doc: Document, section: SectionNode): EvalResult {
        if (section.type === SectionType.Title) {
            return EvalResult.break;
        }

        return EvalResult.continue;
    }
}

import { Markdown } from "@minidoc/core";
import MarkdownSectionNode from '../../parser/nodes/MarkdownSectionNode';

export default class MarkdownInterpreter {

    public evalMarkdown(node: MarkdownSectionNode): Markdown | null {
        if (node !== undefined &&
            node !== null &&
            node.content.length > 0) {

            const markdown = new Markdown(node.content);
            return markdown;
        }

        return null;
    }
}

import { Document } from "@minidoc/core";
import { evaluate } from "./interpreter/Interpreter";
import { lex } from "./lexer/Lexer";
import { parse } from "./parser/Parser";

export function process(input: string): Document | null {
    const tokens = lex(input);
    const ast = parse(tokens);
    const document = evaluate(ast);

    return document;
}

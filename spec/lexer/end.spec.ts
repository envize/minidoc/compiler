import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import EndLexer from '../../src/lexer/sublexers/EndLexer';
import TokenType from '../../src/lexer/TokenType';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import IndentLexer from '../../src/lexer/sublexers/IndentLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';

const end = 'end';

createSuite(tests, lexerSuite, 'end');

function tests(lex: lex) {    

    test('expect end to return token', () => {
        // arrange
        const input = end;

        // act
        const { tokens } = lex(input, [EndLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect end with extra character at the beginning to return nothing', () => {
        // arrange
        const input = `e${end}`;

        // act
        const { tokens } = lex(input, [EndLexer, FreeTextLexer], [TokenType.End]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect end with extra character at the end to return nothing', () => {
        // arrange
        const input = `${end}d`;

        // act
        const { tokens } = lex(input, [EndLexer, FreeTextLexer], [TokenType.End]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect end after whitespace to return token', () => {
        // arrange
        const input = ` ${end}`;

        // act
        const { tokens } = lex(input, [EndLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect end after indent to return token', () => {
        // arrange
        const input = `    ${end}`;

        // act
        const { tokens } = lex(input, [EndLexer, IndentLexer], [TokenType.End]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect end after free text to return nothing', () => {
        // arrange
        const input = `hey ${end}`;

        // act
        const { tokens } = lex(input, [EndLexer, WhitespaceLexer, FreeTextLexer], [TokenType.End]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect end before free text to return token', () => {
        // arrange
        const input = `${end} hey`;

        // act
        const { tokens } = lex(input, [EndLexer, WhitespaceLexer, FreeTextLexer], [TokenType.End]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect end after linebreak to return token', () => {
        // arrange
        const input = `\n${end}`;

        // act
        const { tokens } = lex(input, [EndLexer, NewLineLexer], [TokenType.End]);

        // assert
        expect(tokens).toHaveLength(1);
    });
}
import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import TokenType from '../../src/lexer/TokenType';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import IndentLexer from '../../src/lexer/sublexers/IndentLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';
import StepTypeLexer from '../../src/lexer/sublexers/StepTypeLexer';
import IdentifierLexer from '../../src/lexer/sublexers/IdentifierLexer';

const stepTypes = ['default', 'action'];

createSuite(tests, lexerSuite, 'step-type');

function tests(lex: lex) {

    test.each(stepTypes)
        (`expect step type keyword to return token '%s'`, stepType => {
            // arrange
            const input = stepType;

            // act
            const { tokens } = lex(input, [StepTypeLexer]);

            // assert
            expect(tokens).toHaveLength(1);
        });

    test.each(stepTypes)
        (`expect step type keyword with extra character at the beginning to return nothing '%s'`, stepType => {
            // arrange
            const input = `d${stepType}`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, FreeTextLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(0);
        });

    test.each(stepTypes)
        (`expect step type keyword with extra character at the end to return nothing '%s'`, stepType => {
            // arrange
            const input = `${stepType}t`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, FreeTextLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(0);
        });

    test.each(stepTypes)
        (`expect step type keyword after whitespace to return token '%s'`, stepType => {
            // arrange
            const input = ` ${stepType}`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, WhitespaceLexer]);

            // assert
            expect(tokens).toHaveLength(1);
        });

    test.each(stepTypes)
        (`expect step type keyword after indent to return token '%s'`, stepType => {
            // arrange
            const input = `    ${stepType}`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, IndentLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(1);
        });

    test.each(stepTypes)
        (`expect step type keyword after free text to return nothing '%s'`, stepType => {
            // arrange
            const input = `hey ${stepType}`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, FreeTextLexer, WhitespaceLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(0);
        });

    test.each(stepTypes)
        (`expect step type keyword before free text to return token '%s'`, stepType => {
            // arrange
            const input = `${stepType} hey`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, FreeTextLexer, WhitespaceLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(1);
        });

    test.each(stepTypes)
        (`expect step type keyword after linebreak to return token '%s'`, stepType => {
            // arrange
            const input = `\n${stepType}`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, NewLineLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(1);
        });

    test.each(stepTypes)
        (`expect step type keyword after identifier to return token '%s'`, stepType => {
            // arrange
            const input = `@id ${stepType}`;

            // act
            const { tokens } = lex(input, [StepTypeLexer, IdentifierLexer, WhitespaceLexer], [TokenType.StepType]);

            // assert
            expect(tokens).toHaveLength(1);
        });
}
import { lexerSuite } from '../suites';
import { createSuite, lex } from './lexing';
import StartLexer from '../../src/lexer/sublexers/StartLexer';
import TokenType from '../../src/lexer/TokenType';
import WhitespaceLexer from '../../src/lexer/sublexers/WhitespaceLexer';
import FreeTextLexer from '../../src/lexer/sublexers/FreeTextLexer';
import IndentLexer from '../../src/lexer/sublexers/IndentLexer';
import NewLineLexer from '../../src/lexer/sublexers/NewLineLexer';

const start = 'start';

createSuite(tests, lexerSuite, 'start');

function tests(lex: lex) {    

    test('expect start to return token', () => {
        // arrange
        const input = start;

        // act
        const { tokens } = lex(input, [StartLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect start with extra character at the beginning to return nothing', () => {
        // arrange
        const input = `s${start}`;

        // act
        const { tokens } = lex(input, [StartLexer, FreeTextLexer], [TokenType.Start]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect start with extra character at the end to return nothing', () => {
        // arrange
        const input = `${start}t`;

        // act
        const { tokens } = lex(input, [StartLexer, FreeTextLexer], [TokenType.Start]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect start after whitespace to return token', () => {
        // arrange
        const input = ` ${start}`;

        // act
        const { tokens } = lex(input, [StartLexer, WhitespaceLexer]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect start after indent to return token', () => {
        // arrange
        const input = `    ${start}`;

        // act
        const { tokens } = lex(input, [StartLexer, IndentLexer], [TokenType.Start]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect start after free text to return nothing', () => {
        // arrange
        const input = `hey ${start}`;

        // act
        const { tokens } = lex(input, [StartLexer, WhitespaceLexer, FreeTextLexer], [TokenType.Start]);

        // assert
        expect(tokens).toHaveLength(0);
    });

    test('expect start before free text to return token', () => {
        // arrange
        const input = `${start} hey`;

        // act
        const { tokens } = lex(input, [StartLexer, WhitespaceLexer, FreeTextLexer], [TokenType.Start]);

        // assert
        expect(tokens).toHaveLength(1);
    });

    test('expect start after linebreak to return token', () => {
        // arrange
        const input = `\n${start}`;

        // act
        const { tokens } = lex(input, [StartLexer, NewLineLexer], [TokenType.Start]);

        // assert
        expect(tokens).toHaveLength(1);
    });
}
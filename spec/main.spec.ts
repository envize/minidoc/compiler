import { process } from '../src/index';
import { processorSuite } from './suites';


describe(processorSuite, () => {

    describe('process()', () => {
        
        test('expect valid input to return valid document', () => {
            // arrange
            const input = '#title hey\n#general\nhello';

            // act
            const doc = process(input);
    
            // assert
            expect(doc).not.toBeNull();
            expect(doc).not.toBeUndefined();
            expect(doc.title).toBe('hey');
            expect(doc.general).not.toBeNull();
            expect(doc.general.content).toBe('hello');
        });
    });
});

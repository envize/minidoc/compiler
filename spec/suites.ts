export const lexerSuite = 'lexer';
export const parserSuite = 'parser';
export const interpreterSuite = 'interpreter';
export const processorSuite = 'processor';
export const isolatedSuite = 'isolated';
export const integratedSuite = 'integrated';
export const integratorSuite = 'integrator';
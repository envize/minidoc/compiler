import { lex } from '../../src/lexer/Lexer';
import { parse } from '../../src/parser/Parser';
import { lexerSuite } from '../suites';
import { evaluate as evaluateBase } from '../../src/interpreter/Interpreter';

function evaluate(input: string) {
    const tokens = lex(input);
    const ast = parse(tokens);
    const doc = evaluateBase(ast);

    return doc;
}

describe(lexerSuite, () => {

    describe('main', () => {
        
        test('expect null input to throw error', () => {
            // act
            const sut = () => evaluateBase(null);
    
            // assert
            expect(sut).toThrowError();
        });
        
        test('expect undefined input to throw error', () => {
            // act
            const sut = () => evaluateBase(undefined);
    
            // assert
            expect(sut).toThrowError();
        });
        
        test('expect valid input be evaluated correctly', () => {
            // arrange
            const input = '#title hey\n#general\nhello';

            // act
            const doc = evaluate(input);
    
            // assert
            expect(doc).not.toBeNull();
            expect(doc).not.toBeUndefined();
            expect(doc.title).toBe('hey');
            expect(doc.general).not.toBeNull();
            expect(doc.general.content).toBe('hello');
        });
        
        test('expect document without title to be null', () => {
            // arrange
            const input = '#general\nhello';

            // act
            const doc = evaluate(input);
    
            // assert
            expect(doc).toBeNull();
        });
    });
});
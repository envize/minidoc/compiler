import { interpreterSuite } from '../suites';
import { evaluate } from './evaluation';
import AlternateFlowInterpreter from '../../src/interpreter/subinterpreters/AlternateFlowInterpreter';
import { Document, Flow, FlowType, Step, StepType } from '@minidoc/core';

function mockDocument() {
    const step = new Step(StepType.Default, 'dummy');
    step.id = 'id';

    const doc = new Document('test');
    doc.happyFlow = new Flow(FlowType.HappyFlow);
    doc.happyFlow.steps.push(step);

    return doc;
}

describe(interpreterSuite, () => {

    describe('alternate-flow', () => {

        test('expect alternate flow section to be integrated when valid', () => {
            // arrange
            const input = '#alternate-flow flow\nstep one\naction @id step two';

            // act
            const document = evaluate(AlternateFlowInterpreter, input, mockDocument());

            // assert
            expect(document.alternateFlows.length).toBe(1);
            expect(document.alternateFlows[0].type).toBe(FlowType.AlternateFlow);
            expect(document.alternateFlows[0].description).toBe('flow');
            expect(document.alternateFlows[0].steps.length).toBe(2);
            expect(document.alternateFlows[0].start).toBeNull();
            expect(document.alternateFlows[0].end).toBeNull();
        });

        test('expect links not to be ignored', () => {
            // arrange
            const input = '#alternate-flow flow\nstart @id\nstep one\naction @two step two\nend @id';

            // act
            const document = evaluate(AlternateFlowInterpreter, input, mockDocument());

            // assert
            expect(document.alternateFlows.length).toBe(1);
            expect(document.alternateFlows[0].type).toBe(FlowType.AlternateFlow);
            expect(document.alternateFlows[0].description).toBe('flow');
            expect(document.alternateFlows[0].steps.length).toBe(2);
            expect(document.alternateFlows[0].start).not.toBeNull();
            expect(document.alternateFlows[0].end).not.toBeNull();
        });

        test('expect broken links to be ignored', () => {
            // arrange
            const input = '#alternate-flow flow\nstart @broken\nstep one\naction @two step two\nend @broken';

            // act
            const document = evaluate(AlternateFlowInterpreter, input, mockDocument());

            // assert
            expect(document.alternateFlows.length).toBe(1);
            expect(document.alternateFlows[0].type).toBe(FlowType.AlternateFlow);
            expect(document.alternateFlows[0].description).toBe('flow');
            expect(document.alternateFlows[0].steps.length).toBe(2);
            expect(document.alternateFlows[0].start).toBeNull();
            expect(document.alternateFlows[0].end).toBeNull();
        });

        test.each([
            '#alternate-flow',
            '#alternate-flow\nstart @id',
            '#alternate-flow\nend @id',
            '#alternate-flow\nstart @id\nend @id'
        ])
            (`expect alternate flow section not to be integrated when invalid`, (input: string) => {
                // act
                const document = evaluate(AlternateFlowInterpreter, input);

                // assert
                expect(document.alternateFlows.length).toBe(0);
            });

        test('expect title section to be skipped', () => {
            // arrange
            const input = '#title hey';

            // act
            const document = evaluate(AlternateFlowInterpreter, input);

            // assert
            expect(document.alternateFlows.length).toBe(0);
        });

        test('expect second alternate flow section to be added as well', () => {
            // arrange
            const input = '#alternate-flow first\nfirst\n#alternate-flow second\nsecond';

            // act
            const document = evaluate(AlternateFlowInterpreter, input);

            // assert
            expect(document.alternateFlows.length).toBe(2);
            expect(document.alternateFlows[0].description).toBe('first');
            expect(document.alternateFlows[1].description).toBe('second');
        });
    });
});

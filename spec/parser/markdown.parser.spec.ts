import { parserSuite, isolatedSuite } from '../suites';
import { createContext } from './parsing';
import MarkdownSectionParser from '../../src/parser/subparsers/MarkdownSectionParser';
import SectionCategory from '../../src/parser/nodes/SectionType';
import SectionType from '../../src/parser/nodes/Section';

type SampleContent = {
    input: string;
    result: string;
}

const sampleContent: SampleContent[] = [
    {
        input: 'hey',
        result: 'hey'
    },
    {
        input: 'hey hoi',
        result: 'hey hoi'
    },
    {
        input: '# Title\nhey hoi',
        result: '# Title\nhey hoi'
    },
    {
        input: '# Tab test\n    hey hoi',
        result: '# Tab test\n    hey hoi'
    },
    {
        input: '# Linebreak test\n    hey hoi\n',
        result: '# Linebreak test\n    hey hoi'
    }
];

const markdownSections = [
    SectionType.Story,
    SectionType.General
];

const cartesian = (...a) => a.reduce((a, b) => a.flatMap(d => b.map(e => [d, e].flat())));

const options: { content: SampleContent, type: SectionType, toString: () => string }[] = cartesian(sampleContent, markdownSections)
    .map(item => ({ content: item[0], type: item[1], toString: () => `${item[1]}, ${item[0].input.replace('\n', ' ')}` }));

describe(parserSuite, () => {

    describe('markdown', () => {

        describe(parserSuite, () => {

            test.each(options)
                (`expect content to be parsed correctly '%s'`, options => {
                    // arrange
                    const parser = new MarkdownSectionParser(createContext(`\n${options.content.input}`));

                    // act
                    const result = parser.parse(options.type);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.sectionType).toBe(SectionCategory.Markdown)
                    expect(result.node.content).toBe(options.content.result);
                });

            test.each(options)
                (`expect text after section to be ignored '%s'`, options => {
                    // arrange
                    const parser = new MarkdownSectionParser(createContext(`text to be ignored\n${options.content.input}`));

                    // act
                    const result = parser.parse(options.type);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.sectionType).toBe(SectionCategory.Markdown)
                    expect(result.node.content).toBe(options.content.result);
                });

            test('expect happy flow section to be skipped', () => {
                // arrange
                const parser = new MarkdownSectionParser(createContext(`\n${options[0].content.input}`));

                // act
                const result = parser.parse(SectionType.HappyFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });

            test('expect no content to be skipped', () => {
                // arrange
                const parser = new MarkdownSectionParser(createContext(`\n`));

                // act
                const result = parser.parse(markdownSections[0]);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });
        });
    });
});

import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import { createContext } from './parsing';
import SectionNode from '../../src/parser/nodes/SectionNode';
import SectionType from '../../src/parser/nodes/Section';
import FlowSectionParser from '../../src/parser/subparsers/FlowSectionParser';

function createParentNode() {
    return new SectionNode(SectionType.AlternateFlow, '#alternate-flow');
}

describe(parserSuite, () => {

    describe('flow', () => {

        describe(integratorSuite, () => {

            test('expect flow to be integrated when parsed successfully', () => {
                // arrange
                const parser = new FlowSectionParser(createContext('flow\nstep one'));
                const result = parser.parse(SectionType.AlternateFlow);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.section).not.toBeNull();
                expect(parentNode.section).toStrictEqual(result.node);
            });

            test('expect flow not to be integrated when not parsed', () => {
                // arrange
                const parser = new FlowSectionParser(createContext('hey'));
                const result = parser.parse(SectionType.AlternateFlow);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.section).toBeNull();
            });
        });
    });
});

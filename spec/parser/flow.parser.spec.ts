import { parserSuite, isolatedSuite } from '../suites';
import { createContext } from './parsing';
import FlowSectionParser from '../../src/parser/subparsers/FlowSectionParser';
import SectionType from '../../src/parser/nodes/Section';
import StepType from '../../src/parser/nodes/StepType';

describe(parserSuite, () => {

    describe('flow', () => {

        describe(parserSuite, () => {

            test('expect flow description to be parsed correctly', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`flow\nstep one`));

                // act
                const result = parser.parse(SectionType.AlternateFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.description).toBe('flow');
                expect(result.node.steps.length).toBe(1);
                expect(result.node.steps[0].type).toBe(StepType.Default);
                expect(result.node.steps[0].identifier).toBe(null);
                expect(result.node.steps[0].description).toBe('step one');
            });

            test('expect single step to be parsed correctly', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`\nstep one`));

                // act
                const result = parser.parse(SectionType.HappyFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.description).toBe(null);
                expect(result.node.steps.length).toBe(1);
                expect(result.node.steps[0].type).toBe(StepType.Default);
                expect(result.node.steps[0].identifier).toBe(null);
                expect(result.node.steps[0].description).toBe('step one');
            });

            test('expect empty lines to be skipped', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`\nstep one\n\n`));

                // act
                const result = parser.parse(SectionType.HappyFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.description).toBe(null);
                expect(result.node.steps.length).toBe(1);
                expect(result.node.steps[0].type).toBe(StepType.Default);
                expect(result.node.steps[0].identifier).toBe(null);
                expect(result.node.steps[0].description).toBe('step one');
            });

            test('expect step with only step type to be skipped', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`\nstep one\naction`));

                // act
                const result = parser.parse(SectionType.HappyFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.description).toBe(null);
                expect(result.node.steps.length).toBe(1);
                expect(result.node.steps[0].type).toBe(StepType.Default);
                expect(result.node.steps[0].identifier).toBe(null);
                expect(result.node.steps[0].description).toBe('step one');
            });

            test('expect multiple steps to be parsed correctly', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`\nstep one\nstep two`));

                // act
                const result = parser.parse(SectionType.HappyFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.description).toBe(null);
                expect(result.node.steps.length).toBe(2);
                expect(result.node.steps[0].type).toBe(StepType.Default);
                expect(result.node.steps[0].identifier).toBe(null);
                expect(result.node.steps[0].description).toBe('step one');
                expect(result.node.steps[1].type).toBe(StepType.Default);
                expect(result.node.steps[1].identifier).toBe(null);
                expect(result.node.steps[1].description).toBe('step two');
            });

            test('expect flow with no steps to be consumed and skipped', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`\n`));

                // act
                const result = parser.parse(SectionType.HappyFlow);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });

            test('expect story section to be skipped', () => {
                // arrange
                const parser = new FlowSectionParser(createContext(`\n`));

                // act
                const result = parser.parse(SectionType.Story);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });
        });
    });
});

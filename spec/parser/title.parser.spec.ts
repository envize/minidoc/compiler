import { parserSuite, isolatedSuite } from '../suites';
import TitleSectionParser from '../../src/parser/subparsers/TitleSectionParser';
import SectionType from '../../src/parser/nodes/Section';
import { createContext } from './parsing';

describe(parserSuite, () => {

    describe('title', () => {

        describe(parserSuite, () => {

            test('expect single word title to be parsed', () => {
                // arrange
                const parser = new TitleSectionParser(createContext('hey'));

                // act
                const result = parser.parse(SectionType.Title);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.sectionType).toBe(SectionType.Title);
                expect(result.node.title).toBe('hey');
            });

            test('expect multi word title to be parsed', () => {
                // arrange
                const parser = new TitleSectionParser(createContext('hey hoi'));

                // act
                const result = parser.parse(SectionType.Title);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.sectionType).toBe(SectionType.Title);
                expect(result.node.title).toBe('hey hoi');
            });

            test('expect empty title not to be parsed', () => {
                // arrange
                const parser = new TitleSectionParser(createContext(''));

                // act
                const result = parser.parse(SectionType.Title);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).toBeNull();
            });

            test('expect section content to be skipped', () => {
                // arrange
                const parser = new TitleSectionParser(createContext('hey\n'));

                // act
                const result = parser.parse(SectionType.Title);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.sectionType).toBe(SectionType.Title);
                expect(result.node.title).toBe('hey');
            });

            test('expect story section not to be parsed', () => {
                // arrange
                const parser = new TitleSectionParser(createContext('hey'));

                // act
                const result = parser.parse(SectionType.Story);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });
        });
    });
});

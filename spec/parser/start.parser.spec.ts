import { parserSuite, isolatedSuite } from '../suites';
import LinkType from '../../src/parser/nodes/LinkType';
import { createContext } from './parsing';
import StartParser from '../../src/parser/subparsers/StartParser';

const start = 'start';
const id = '@id';

describe(parserSuite, () => {

    describe('start', () => {

        describe(parserSuite, () => {

            test('expect start followed by id token to be parsed', () => {
                // arrange
                const parser = new StartParser(createContext(`${start} ${id}`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(LinkType.Start);
                expect(result.node.identifier).toBe(id);
                expect(result.node.description).toBeNull();
            });

            test('expect start followed by id and description token to be parsed', () => {
                // arrange
                const parser = new StartParser(createContext(`${start} ${id} multi word description`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeTruthy();
                expect(result.breakFromMainLoop).toBeTruthy();
                expect(result.node).not.toBeUndefined();
                expect(result.node).not.toBeNull();
                expect(result.node.type).toBe(LinkType.Start);
                expect(result.node.identifier).toBe(id);
                expect(result.node.description).toBe('multi word description');
            });

            test('expect start without id token not to be parsed', () => {
                // arrange
                const parser = new StartParser(createContext(start));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });

            test('expect id followed by start token not to be parsed', () => {
                // arrange
                const parser = new StartParser(createContext(`${id} ${start}`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });
        });
    });
});

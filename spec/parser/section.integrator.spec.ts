import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import { createContext } from './parsing';
import SectionParser from '../../src/parser/subparsers/SectionParser';
import AbstractSyntaxTree from '../../src/parser/AbstractSyntaxTree';

function createParentNode() {
    return new AbstractSyntaxTree();
}

describe(parserSuite, () => {

    describe('section', () => {

        describe(integratorSuite, () => {

            test('expect section to be integrated when parsed successfully', () => {
                // arrange
                const parser = new SectionParser(createContext('#title hey'));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.sections.length).toBe(1);
                expect(parentNode.sections[0]).toStrictEqual(result.node);
            });

            test('expect section not to be integrated when not parsed', () => {
                // arrange
                const parser = new SectionParser(createContext('hey'));
                const result = parser.parse(undefined);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.sections.length).toBe(0);
            });
        });
    });
});

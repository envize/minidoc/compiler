import { integratorSuite } from '../suites';
import { parserSuite } from '../suites';
import { createContext } from './parsing';
import SectionNode from '../../src/parser/nodes/SectionNode';
import SectionType from '../../src/parser/nodes/Section';
import TitleSectionParser from '../../src/parser/subparsers/TitleSectionParser';

function createParentNode() {
    return new SectionNode(SectionType.Title, '#title');
}

describe(parserSuite, () => {

    describe('title', () => {

        describe(integratorSuite, () => {

            test('expect title to be integrated when parsed successfully', () => {
                // arrange
                const parser = new TitleSectionParser(createContext('hey'));
                const result = parser.parse(SectionType.Title);
                const parentNode = createParentNode();

                // info: hey
                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.section).not.toBeNull();
                expect(parentNode.section).toStrictEqual(result.node);
            });

            test('expect title not to be integrated when not parsed', () => {
                // arrange
                const parser = new TitleSectionParser(createContext(''));
                const result = parser.parse(SectionType.Title);
                const parentNode = createParentNode();

                // act
                parser.integrate(result, parentNode);

                // assert
                expect(parentNode.section).toBeNull();
            });
        });
    });
});

import { parserSuite, isolatedSuite } from '../suites';
import { createContext } from './parsing';
import SectionParser from '../../src/parser/subparsers/SectionParser';
import SectionCategory from '../../src/parser/nodes/SectionType';
import SectionType from '../../src/parser/nodes/Section';

const options = [
    {
        type: SectionType.Title,
        section: '#title',
        input: 'hey'
    },
    {
        type: SectionType.General,
        section: '#general',
        input: '\nhey'
    },
    {
        type: SectionType.Story,
        section: '#story',
        input: '\nhey'
    },
    {
        type: SectionType.HappyFlow,
        section: '#happy-flow',
        input: '\nhey'
    },
    {
        type: SectionType.AlternateFlow,
        section: '#alternate-flow',
        input: '\nhey'
    },
];

options.forEach(option => option.toString = () => option.section);

describe(parserSuite, () => {

    describe('section', () => {

        describe(parserSuite, () => {

            test.each(options)
                (`expect section to be parsed correctly '%s'`, options => {
                    // arrange
                    const parser = new SectionParser(createContext(`${options.section} ${options.input}`));

                    // act
                    const result = parser.parse(undefined);

                    // assert
                    expect(result.shouldBeIntegrated).toBeTruthy();
                    expect(result.breakFromMainLoop).toBeTruthy();
                    expect(result.node).not.toBeUndefined();
                    expect(result.node).not.toBeNull();
                    expect(result.node.type).toBe(options.type);
                    expect(result.node.keyword).toBe(options.section);
                });

            test(`expect invalid section to be skipped`, () => {
                // arrange
                const parser = new SectionParser(createContext(`#hey\nhallo`));

                // act
                const result = parser.parse(undefined);

                // assert
                expect(result.shouldBeIntegrated).toBeFalsy();
                expect(result.breakFromMainLoop).toBeFalsy();
                expect(result.node).toBeUndefined();
            });
        });
    });
});
